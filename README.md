# Slashdeploy Podcast

_NOTE: The podcast is currently under pre-production. Nothting has
been published yet. Work is on going to publish the first set of
episodes._

> A podcast on building, deploying, and running software.

See the [issues][] and [wiki][] for more information.

## Goals

The goal is simple: help others improve at building, deploying, and
running software. Each episodes focuses on a set of specific outcomes
and action points. This way the audience sees a clear path to whatever
their improvement goal is. If they're not into that, then informed
highly discussions will clarify the problem space so that they take
the next step. Ultimately the audience should feel more equipped to
design and build systems, deploying more often with more confidence,
and work with systems that run themselves.

## Format

The podcast will start out with a single host with guests, book
reviews, and special comment episodes. Hopefully recurring guests may
become recurring hosts and thus turn the show into panel form.
Regardless of that happening, there will always be "guest-less"
episodes with just the hosts discussing a topic.

## Getting Involved

Audience collaboration is more than encouraged! The podcast is for you
so get involved and let me know what you're interested in. You can
request topics or volunteer yourself to come on the show by opening
an [issue][issues].

Episodes are tracked on the [project board][]. Material for each
episode is development on the [wiki][] with participation from the
guests/hosts.

[issues]: https://gitlab.com/slashdeploy/podcast/issues
[wiki]: https://gitlab.com/slashdeploy/podcast/wikis/home
[project board]: https://gitlab.com/slashdeploy/podcast/boards
